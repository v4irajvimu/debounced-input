# debounced-input

> Wait for the typing ends and invoke passed callback. lodash debounce implementation with react hooks.

[![NPM](https://img.shields.io/npm/v/debounced-input.svg)](https://www.npmjs.com/package/debounced-input) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save debounced-input
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'debounced-input'

class Example extends Component {
  render () {
    return (
      <MyComponent />
    )
  }
}
```

## License

MIT © [v4irajvimu](https://github.com/v4irajvimu)
