import React, { useState, useEffect, useRef } from "react";
import classNames from "classnames";
import _ from "lodash";

export default function TextInput(props) {
  const {
    label,
    value,
    placeholder,
    type = "text",
    hasError = false,
    hasErrorMsg = "",
    inputOnly = false,
    HandleOnChange
  } = props;

  let minMax = {};
  if (type === "number") {
    if (props.min) {
      minMax = {
        ...minMax,
        min: Number(props.min)
      };
    }
    if (props.max) {
      minMax = {
        ...minMax,
        max: Number(props.max)
      };
    }
  }
  const [InputValue, setInputValue] = useState(value);
  const [hasFormError, setHasFormError] = useState(hasError);
  const [hasFormErrorMsg, setHasFormErrorMsg] = useState(hasErrorMsg);

  useEffect(() => {
    setHasFormError(hasError);
    setHasFormErrorMsg(hasErrorMsg);
    return () => {
      setHasFormError(false);
      setHasFormErrorMsg("");
    };
  }, [hasError, hasErrorMsg]);

  const updateValue = useRef(
    _.debounce(val => {
      HandleOnChange && HandleOnChange(val);
    }, 300)
  ).current;

  return inputOnly ? (
    <input
      type={type}
      className="form-control"
      placeholder={placeholder}
      onChange={e => {
        setInputValue(e.target.value);
        updateValue(e.target.value);
      }}
      value={InputValue}
      {...minMax}
    />
  ) : (
    <div className={classNames("form-group", { "has-error": hasFormError })}>
      {type !== "hidden" && <label>{label}</label>}
      <input
        type={type}
        className="form-control"
        placeholder={placeholder}
        // onChange={e => HandleOnChange && HandleOnChange(e.target.value)}
        onChange={e => {
          setInputValue(e.target.value);
          updateValue(e.target.value);
        }}
        value={InputValue}
        {...minMax}
      />
      {hasFormError && <span class="help-block">{hasFormErrorMsg}</span>}
    </div>
  );
}
